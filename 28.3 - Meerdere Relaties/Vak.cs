﻿namespace _28._3___Meerdere_Relaties
{
    internal class Vak
    {
        private string _beschrijving;
        private string _leslokaal;
        private int _lesuren;
        public Vak(string beschrijving, string leslokaal, int lesuren)
        {
            Beschrijving = beschrijving;
            Leslokaal = leslokaal;
            Lesuren = lesuren;
        }
        public string Beschrijving { get { return _beschrijving; } set { _beschrijving = value; } }
        public string Leslokaal { get { return _leslokaal; } set { _leslokaal = value; } }
        public int Lesuren { get { return _lesuren; } set { _lesuren = value; } }
        public override string ToString()
        {
            return Beschrijving + " - " + Lesuren + " Uren - " + Leslokaal;
        }
        public override bool Equals(object? obj)
        {
            bool resultaat = false;
            if (obj != null)
            {
                if (GetType() == obj.GetType())
                {
                    Vak g = (Vak)obj;
                    if (this.Beschrijving == g.Beschrijving)
                    {
                        resultaat = true;
                    }
                }
            }
            return resultaat;
        }

    }
}
