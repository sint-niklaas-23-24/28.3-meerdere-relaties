﻿namespace _28._3___Meerdere_Relaties
{
    internal class Docent
    {
        private string _naam;
        private List<Vak> _vakken;
        public Docent(string naam)
        {
            Naam = naam;
            Vakken = new List<Vak>();
        }
        public string Naam { get { return _naam; } set { _naam = value; } }
        private List<Vak> Vakken { get { return _vakken; } set { _vakken = value; } }
        public void AddVak(Vak vak)
        {
            bool vakGelijk = false;
            foreach (Vak eenVak in Vakken)
            {
                if (vak.Equals(eenVak))
                {
                    Console.WriteLine("Vak bestaat al! U kan deze niet opnieuw toevoegen");
                    vakGelijk = true;
                }
            }
            if (!vakGelijk)
            {
                Vakken.Add(vak);
            }
        }
        public void RemoveVak(Vak vak)
        {
            foreach (Vak eenVak in Vakken)
            {
                if (vak == eenVak)
                {
                    Vakken.Remove(eenVak);
                    break;
                }
            }
        }
        public override string ToString()
        {
            string display;
            display = Naam + " geeft de volgende vakken: " + Environment.NewLine;
            foreach (Vak vak in Vakken)
            {
                display += vak.ToString() + Environment.NewLine; //Checken sebiet of newline nodig is
            }
            return display;
        }
    }
}
