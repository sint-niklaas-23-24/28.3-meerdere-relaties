﻿namespace _28._3___Meerdere_Relaties
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Docent hvs = new Docent("Hans");
            Vak Cscherp = new Vak("Basic C#", "LOK1", 11);
            Vak Cscherper = new Vak("Advanced C#", "LOK1", 13);
            Vak Web = new Vak("Web", "LOK7", 12);
            Vak zorgeloosCoderen = new Vak("GIT (Zorgeloos coderen)", "LOK9", 2);
            hvs.AddVak(Cscherp);
            hvs.AddVak(Cscherper);
            hvs.AddVak(Web);
            hvs.AddVak(zorgeloosCoderen);
            Console.WriteLine(hvs.ToString());
            Console.ReadLine();
            hvs.RemoveVak(Web);
            Console.WriteLine("Na het verwijderen van web");
            Console.Read();
            Console.WriteLine(hvs.ToString());


        }
    }
}
